# Windows Development Files for Wireshark (x64)

## Overview

This repository contains the libraries necessary to compile Wireshark for
64-bit Windows. You can download and unpack them by hand, but it's much
easier to run `cmake` from the main Wireshark source directory, which
automatically downloads the files your local library directory and unpacks
them.


## Packages

The packages in this repository came from a number of locations and have
been released under different licenses.


### Package: vcpkg-export-20220726-1-win64ws.zip

Location: https://github.com/Microsoft/vcpkg  
Description: Windows library bundle created using `vcpkg export`  
Included libraries, versions, and licenses:
  * Dirent 1.23.2, MIT  
  * Gettext 0.21 (provides intl, LGPL-2.1-or-later)  
  * GLib2 2.72.3, LGPL-2.1-or-later  
  * Libffi 3.4, MIT  
  * Libiconv 1.17, GPL-3.0-or-later & LGPL-2.1-or-later  
  * Liblzma 5.2.5, public domain  
  * Libxml2 2.9.14, MIT  
  * PCRE 8.45, BSD  
  * Zlib 1.2.12, zlib/libpng
  
CRT: Universal CRT  
Local changes: Created using https://gitlab.com/wireshark/wireshark-vcpkg-scripts  

### Package: bcg729-1.1.1-1-win64ws.zip

Location: http://www.belledonne-communications.com/bcg729.html  
Description: bcg729 is a G.729A decoder / encoder  
License: GNU GPLv2  
CRT: MSVCRT.DLL  
Local changes: Created using https://gitlab.com/wireshark/wireshark-vcpkg-scripts  

### Package: brotli-1.0.9-1-win64ws.zip

Location: https://github.com/google/brotli  
Description: Brotli is lossless compression algorithm  
License: MIT  
CRT: Universal CRT  
Local Change: Compiled using vcpkg and Visual Studio 2019  

### Package: c-ares-1.18.1-1-win64ws.zip

Location: http://c-ares.haxx.se/  
Description: Asynchronous DNS resolver library.  
License: MIT  
CRT: Universal CRT  
Local changes: Created using https://gitlab.com/wireshark/wireshark-vcpkg-scripts  

### Package: gnutls-3.6.3-1-win64ws.zip

Location: https://git.lekensteyn.nl/peter/wireshark-notes/tree/windows-libs/make-gnutls-libs-zip.sh?id=b86878e458d5d7deb21218ce6598b98af4ed7ec2  
Description: Fedora 29 MinGW64 packages including gnutls, gcc, gmp, nettle, p11-kit, libffi, libtasn1 and winpthreads.  
License: GNU LGPL  
CRT: MSVCRT.DLL  
Local changes: The package was created as described in README.Wireshark. (Basically, copy upstream .dll and include, create .lib)  
Packages: krb5-1.17-1-win64ws.zip  
Location: https://web.mit.edu/kerberos/  
Decription: Kerberos 1.17 built using Visual C++ 2017  
Licence: MIT  
CRT: Universal CRT  
Local changes: The package was created as described in README.Wireshark.  

### Package: libgcrypt-1.10.1-2-win64ws.zip

Location: https://gnupg.org/download/index.html#libgcrypt  
Description: Libgcrypt encryption library plus libgpg-error.  
License: GNU LGPL and GPL  
CRT: MSVCRT.DLL  
Local changes: The package was created from Debian's packages as described in README.Wireshark.  

### Package: libilbc-2.0.2-3-win64ws

Location: https://github.com/ShiftMediaProject/libilbc/releases  
Description: iLBC decoder / encoder  
License: BSD  
CRT: Universal CRT  
Local changes: The package was created as described in README.Wireshark.  

### Package: libmaxminddb-1.4.3-1-win64ws.zip

Location: https://github.com/maxmind/libmaxminddb  
Description: GeoIP2 geolocation and database lookup library.  
License: Apache License 2.0  
CRT: Universal CRT (static)  
Local Change: Compiled using vcpkg and Visual Studio 2019  

### Package: libpcap-1.10.1-1-win64ws

Location: https://www.tcpdump.org/  
Description: Packet capture library  
License: BSD-3-Clause  
CRT: Universal CRT  
Local Change: Compiled using vcpkg and Visual Studio 2019  

### Package: lua-5.2.4_Win64_dllw4_lib.zip

Location: http://luabinaries.sourceforge.net/  
Description: The Lua scripting language  
License: MIT  
CRT: MSVCRT.DLL  
Local changes: lua52.def generated with Dependency Walker and lua52.lib generated with MSVC lib  

### Package: lua-5.2.4-unicode-win64-vc14.zip

Location: https://github.com/Lekensteyn/lua-unicode  
Description: The Lua scripting language (patched for UTF-8 support on Windows).  
License: MIT  
CRT: Universal CRT  
Local changes: msvc2017 version taken from the releases page, 5.2.4-release1 (commit dbcb2a80)  

### Package: lua-5.3.6-unicode-win64-vc14-1.zip

Location: https://gitlab.com/jvalverde/lua53-wireshark-devlibs  
Description: Fork of github.com/Lekensteyn/lua-unicode for Lua 5.3.  
License: MIT  
CRT: Universal CRT  
Local changes: None.  

### Package: lz4-1.9.3-1-win64ws.zip

Location: https://lz4.github.io/lz4  
Description: LZ4 is lossless compression algorithm  
License: BSD  
CRT: Universal CRT  
Local Change: Compiled using vcpkg and Visual Studio 2019  

### Package: nghttp2-1.49.0-1-win64ws.zip

Location: https://www.nghttp2.org  
Description: nghttp2 is an implementation of HTTP/2 and its header compression algorithm HPACK in C  
License: MIT  
CRT: Universal CRT  
Local changes: Created using https://gitlab.com/wireshark/wireshark-vcpkg-scripts  

### Package: nghttp3-0.7.0-1-win64ws.zip

Location: https://github.com/ngtcp2/nghttp3  
Description: nghttp3 is an implementation of RFC 9114 HTTP/3 mapping over QUIC and RFC 9204 QPACK in C  
License: MIT  
CRT: Universal CRT  
Local changes: Created using https://gitlab.com/wireshark/wireshark-vcpkg-scripts  

### Package: pcre2-10.40-1-win64ws.zip

Location: https://www.pcre.org/  
Description: Perl Compatible Regular Expression library  
License: BSD  
CRT: Universal CRT  
Local changes: Created using https://gitlab.com/wireshark/wireshark-vcpkg-scripts  

### Package: snappy-1.1.9-1-win64ws.zip

Location: https://google.github.io/snappy  
Description: Snappy is a compression/decompression library  
License: BSD  
CRT: Universal CRT  
Local Change: Compiled using vcpkg and Visual Studio 2019  

### Package: AirPcap_Devpack_4_1_0_1622.zip

Location: http://www.cacetech.com/  
Description: The AirPcap developer's pack  
License: BSD  
CRT: MSVCRT.DLL  
Local changes: None.  

### Packages: user-guide-gdf2fcdf.zip

Location: https://www.wireshark.org/docs/  
Description: Wireshark User's Guide in Microsoft's "Compressed HTML Help" format  
License: GPL  
Local Change: None.  

### Package: libsmi-svn-40773-win32ws.zip

Location: http://www.ibr.cs.tu-bs.de/projects/libsmi/  
Description: a library that allows management applications to access SMI MIB module definitions.  
License: libsmi (http://www.ibr.cs.tu-bs.de/projects/libsmi/COPYING.html)  
Local changes: The package was created as described in README.Wireshark.  

### Package: FindProc.zip

Location: http://nsis.sourceforge.net/Find_Process_By_Name  
Description: NSIS plugin to determine if a given executable (name) is running. Used by PortableApps distribution.  
License:  FindProc (http://nsis.sourceforge.net/Find_Process_By_Name#Copyright)  
Local Change: None  

### Package: npcap-1.71.exe

Location: https://nmap.org/npcap/  
Description: Windows packet capture driver  
License: Npcap. https://raw.githubusercontent.com/nmap/npcap/master/LICENSE  
Local changes: None  

### Package: USBPcapSetup-1.5.4.0.exe

Location: https://github.com/desowin/usbpcap  
Description: Windows USB packet capture driver  
License: GPLv2 for USBPcapDriver and BSD 2-Clause for USBPcapCMD  
Local changes: None  

### Package: WinSparkle-0.5.7.zip

Location: http://www.winsparkle.org/  
Description: Windows software update library  
License: MIT  
Local changes: None.  

### Package: libssh-0.9.5-win64ws.zip

Location: https://www.libssh.org  
Description: Multiplatform C library implementing the SSHv2 and SSHv1 protocol on client and server side  
License: GNU LGPL  
CRT: Universal CRT  
Local Change: Compiled using vcpkg and Visual Studio 2019  

### Package: sbc-1.3-1-win64ws.zip

Location: https://www.kernel.org/pub/linux/bluetooth  
Description: SBC is a digital audio encoder and decoder used to transfer data to Bluetooth audio output devices like headphones or loudspeakers  
License: GNU GPLv2  
CRT: MSVCRT.DLL  
Local changes: None  

### Package: spandsp-0.0.6-2-win64ws.zip

Location: http://www.soft-switch.org/  
Description: SpanDSP is a library of DSP functions for telephony  
License: GNU LGPLv2.1  
CRT: MSVCRT.DLL  
Local changes: Removed FAX related files to decrease library size. See README.Wireshark file in archive for details  

### Package: minizip-1.2.11-4-win64ws.zip

Location: https://github.com/microsoft/vcpkg/tree/master/ports/minizip  
Description: minizip is a zip manipulation library written in C  
License: zlib license  
CRT: Universal CRT  
Local changes: The package was created as described in README.Wireshark  

### Package: zstd-1.5.2-1-win64ws.zip

Location: https://github.com/microsoft/vcpkg/tree/master/ports/zstd  
Description: Zstandard is a real-time compression algorithm, providing high compression ratios.  
License: BSD  
CRT: Universal CRT  
Local changes: The package was created as described in README.Wireshark  

### Package: opus-1.3.1-3-win64ws.zip

Location: https://github.com/microsoft/vcpkg/tree/master/ports/opus  
Description: Opus is a codec for interactive speech and audio transmission over the Internet.  
License: BSD  
CRT: Universal CRT  
Local changes: None  

### Package: speexdsp-1.21.1-1-win64ws.zip

Location: https://www.speex.org
Description: Speex is an Open Source/Free Software patent-free audio compression format designed for speech.
License: BSD
CRT: Universal CRT
Local changes: Created using https://gitlab.com/wireshark/wireshark-vcpkg-scripts
